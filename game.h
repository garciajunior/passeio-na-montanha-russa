#ifndef GAME_H_
#define GAME_H_

// funcao responsavel pela execução do jogo
void start();

// Inicia do ponto A até o ponto B

void fase1(float mass, float height, float gravity, float initialSpeed);

// Inicia do ponto B ate o ponto C, fazendo os ajustes necessários na massa
void fase2(float mass, float height, float hCD, float gravity, float initialSpeed);

//Inicia no ponto D e vai ate o ponto, fazendo os ajustes necessários na gravidade
void fase3(float mass, float height, float hEF, float gravity);

//inicia no ponto F  ate o ponto G fazendo o ajuste na massa
void fase4(float mass, float height, float hGF, float gravity);

// inicia no ponto h e vai ate I, no qual e feito o calculo do atrito 
//e tempo de parada do carrinho no trillo
void fase5(float mass, float gravity, float hGH, float s);

//validação da conservação da energia 
void validationMechanicEnergy(float kinetic, float potential);


//Caso o jogador na faz os ajustes necessarios
void gameOver();

//funcao responsavel pela entradas dos ajustes dado pelo usuario
float question(char variable[10], float value);

#endif