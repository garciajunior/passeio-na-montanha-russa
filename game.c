#include <stdio.h>
#include "game.h"
#include "phisics.h"
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "message.h"

//Funcao de inicio dom jogo
void start()
{
  float mass = 0, height = 0, initialSpeed = 0, gravity = 10;
  float hCD = 0, hEF = 0, hGF = 0, hGH = 0, deltaSpace = 0;
  float speedCar = 0, massCar;
  //Imprime desenho da montanha russa e o nome do jogo
  printRoller();
  gameName();

  // Imputs do jogador
  printf("No ponto inicial informe a velocidade do primeiro carrinho: \t");
  scanf("%f", &speedCar);
  printf("Insira um valor para a massa do  primeiro carrinho em kilos:\t ");
  scanf(" %f", &massCar);

  printf("Insira um valor para o altura inicial da montanha russa em metros:\t");
  scanf(" %f", &height);

  //Calculo da massa do carrinho que ira percorrer a montanha  russa.
  mass = findMass(massCar);
  initialSpeed = initialSpeedCar(mass, massCar, speedCar);

  //Calculo das alturas, nas devidas proporções dado pelo enuciado do exercicio.
  hCD = (5 * height) / 4;
  hEF = (3 * height) / 4;
  hGF = (height / 2);
  hGH = (0.5 - 0.375) * height;
  //distancia de H ate I percorrida ate o carrinho ter sua velocidade final igual a 0
  deltaSpace = 3 * height;

  printf("Dados iniciais:\n");
  printf("\tAltura no ponto A = %.2f \n", height);
  printf("\tAltura no ponto CD = %.2f\n", hCD);
  printf("\tAltura no ponto EF = %.2f\n", hEF);
  printf("\tAltura no ponto GF = %.2f\n", hGF);
  printf("\tAltura no ponto GH = %.2f\n", hGH);
  printf("\tDistancia no ponto HI = %.2f\n", deltaSpace);
  printf("\tGravidade no ponto A = %.2f\n", gravity);
  printf("\tVelocidade inicial A = %.2f\n", initialSpeed);
  printf("________________________________________________\n\n\n");

  // Fases do jogo
  fase1(mass, height, gravity, initialSpeed);

  fase2(mass, height, hCD, gravity, initialSpeed);
  printf("\n---------------------------------------------------------------\n");
  fase3(mass, hCD, hEF, gravity);
  printf("\n---------------------------------------------------------------\n");
  fase4(mass, hEF, hGF, gravity);
  printf("\n---------------------------------------------------------------\n");
  fase5(mass, gravity, hGH, deltaSpace);
}
void fase1(float mass, float height, float gravity, float initialSpeed)
{
  //Calculo da velocidade no ponto B
  float speed = speedGravity(initialSpeed, gravity, height);
  printf("\tNo ponto B a velocidade final  = %.2f\n", speed);
  printf("\tValor da energia potential no ponto A , %.2f\n", potentialEnergy(300, 10, height));
  printf("\tValor da energia cinetica no ponto B = %.2f\n", kineticEnergy(mass, speed));
}
void fase2(float mass, float height, float hCD, float gravity, float initialSpeed)
{

  //Calculo da velocidade no ponto B
  float speed = speedGravity(initialSpeed, gravity, height);
  float value = 0;
  //Calculo da energia cinetica no ponto B
  float kinetic = kineticEnergy(mass, speed);
  float potential = potentialEnergy(mass, gravity, hCD);
  float newMass = 0;
  //encontrado o valor ideal para indicar ao jogador qual valor é o mais aproprioado
  value = ((2 * potential / pow(speed, 2)) - mass);

  printf("\tEnergia Cinetica no ponto B = %.2f \n", kinetic);
  printf("\tEnergia potential no ponto C:  %.2f \n", potential);

  printf("\tValor da massa  atual é %f\n", mass);
  //Pergunta o valor do ajuste para o jogador
  newMass = mass + question("massa", value);
  kinetic = kineticEnergy(newMass, speed);
  potential = potentialEnergy(mass, gravity, hCD);

  printf("\nCom os ajustes realizados houve alteração na Energia Cinetica em B: %.2f \n", kinetic);
  printf("\nAgora a Energia potential em C:  %.2f \n", potential);

  // Verifica se o ajuste foi bom ou ruim
  validationMechanicEnergy(kinetic, potential);
}

void fase3(float mass, float height, float hEF, float gravity)
{
  float newGravity = 0;
  float potential = 0;
  float kinetic = 0;

  potential = potentialEnergy(mass, gravity, hEF);
  //Encontrado o valor da gravidade ideal
  newGravity = (potential) / (hEF * mass);

  printf("\tEnergia potencial em E = %.2f\n", potential);

  gravity += question("gravidade", newGravity);

  float newSpeed = speedGravity(0, gravity, 25);
  potential = potentialEnergy(mass, gravity, hEF);
  kinetic = kineticEnergy(mass, newSpeed);

  printf("\tApos a atualização da gravidade, a velocidade em D é: %.2f\n", newSpeed);
  printf("\tA nova energia potencial em E = %.2f\n", potential);
  printf("\tA nova energia cinetica em D = %.2f\n", kinetic);

  // validação da conservação da energia

  validationMechanicEnergy(kinetic, potential);
}
void fase4(float mass, float height, float hGF, float gravity)
{
  float speed = speedGravity(0, 10, hGF);
  float potential = potentialEnergy(mass, gravity, hGF);
  float newMass = 0;
  float kinetic = 0;
  //Encontrando o valor ideal da massa
  newMass = (((2 * potential) / (pow(speed, 2))) - mass);

  printf("\tA velocidade em F = %.2f\n", speed);
  printf("\tnNeste instante a massa inicial do carrinho permanece a mesma  %.2f Kg\n", mass);
  printf("\tnA gravidade já retornou ao normal e seu valor e %.2f m/s \n", gravity);

  mass += question("massa", newMass);
  printf("\tApos o ajuste da massa, seu novo valor e %.2f Kg\n", mass);
  potential = potentialEnergy(mass, gravity, hGF);
  kinetic = kineticEnergy(mass, speed);

  printf("\nApos o ajuste da massa a Energia cinetica em F= %.2f J\n", kineticEnergy(mass, speed));
  printf("\nE a energia potencial em G =  %.2f\n", potentialEnergy(mass, gravity, hGF));

  // validação da conservação da energia
  validationMechanicEnergy(kinetic, potential);
}

void fase5(float mass, float gravity, float hGH, float deltaSpace)
{
  // velocidade inicial no ponto H
  float speed = speedGravity(0, gravity, hGH);
  // Energia cinetica no ponto H
  float kinetic = kineticEnergy(gravity, speed);
  //Aceleracao do carrinho 
  float acceleration = accellerationPertorricelli(speed, 0, deltaSpace);
  //Tempo ate sua parada
  float time = timeToStop(0, speed, acceleration);
  // Força de atrito em relacao a normal
  float forceN = force(mass, acceleration);
  //Coeficiente de atrito
  float forceAtr = fatD(forceN, mass, gravity);

  printf("\n Velocidade do H tendendo a 0 ate o carrinho parar pelo atrito da areia nos trilhos = %.2f m/s", speed);
  printf("\n Energia cinetica do carrinho em H= %2.f J", kinetic);
  printf("\n Aceleração que o carrinho se desloca  %f m/s", acceleration);
  printf("\n Tempo gasto para o carrinho parar com  t = %2.f s", time);
  printf("\n Forca normal  F= %2.f N", forceN);
  printf("\n Coeficiente de atrito dinamico Mi = %f", forceAtr);
  printf("\n\n\n\n");
}

void validationMechanicEnergy(float kinetic, float potential)
{
  //arredondando os valores da energia para ignorar depois da quarta casa decimal 
  int roundKinetic = (int)kinetic;
  int roundPotential = (int)potential;

  if (roundKinetic == roundPotential)
  {
    printf("\n\t\t\t\tSeu ajuste foi perfeito! Bom trabalho!!\n");
    return;
  }
  if (roundKinetic > roundPotential)
  {
    printf("\n\t\t\t\tSeu ajuste foi alto demais, voce decolou!!\n");
    gameOver();
  }
  else
  {
    if (roundKinetic < roundPotential)
      ;
    {
      printf("\n\t\t\t\tSeu ajuste não o suficiente o carrinho nao  conseguiu subir a montanha russa!!\n");
      gameOver();
    }
  }
}

void gameOver()
{
  printf("Fim do Jogo!");
  return exit(1);
}

//Retorna o valor do input do jogador
float question(char variable[10], float value)
{
  float adjust = 0;
  char answer;
  printf("\nPara continuar o jogo e necessario um ajuste na [%s] do carrinho, para evitar que o carrinho decole\n", variable);
  printf("\nVoce deseja fazer esse ajuste?\t Insira [s] para continuar  e qualquer tecla para abortar: \t");
  scanf(" %c", &answer);
  if (answer != 's')
  {
    printf("Sua resposta foi %c", answer);
    gameOver();
  }
  printf("Informe o valor do(a) [%s] a ser ajustado! \n\n[DICA: o valor deveria ser proximo de %.4f:\t", variable, value);
  scanf("%f", &adjust);
  printf("\n\n");
  return adjust;
}
