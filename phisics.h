#ifndef PHISICS_H_
#define PHISICS_H_  

/*
Calcula a velocidade baseado na gravidade e altura utilizando a formula Velocidade_final = (velocidade_inicial^2  + 2 * gravidade * altura)
Unidade de retorno em metro/segundo
*/
float speedGravity(float initialSpeed, float gravity, float height);

/*
Calculo da energia cinetica com a formula EC = ((m * velocidade^2 ) / 2)
Unidade de retorno em Joules
*/
float kineticEnergy(float mass, float speed);

/*
Calculo da energia potencial gravitational com a formula Ep = (massa * gravidade * altura)
Unidade de retorno em Joules
*/
float potentialEnergy(float mass, float gravity, float height);

/*
Calculo da aceleração utilizando a formula de Torricelli  Velocidade_final^2 = (velocidade_inicial^2 + 2 * gravidade * altura)
Unidade de retorno em metro/segundos
*/
float accellerationPertorricelli(float initialSpeed, float finalSpeed, float deltaSpace);

/*
Calculo do tempo de parada. Quantos segundos o carrinho leva para sua velocidade final ir para 0
  formula velocidade_final = velocidade_inicial + aceleração * tempo
*/
float timeToStop(float initialSpeed, float finalSpeed, float acceleration);

/*
Calculo da Força  utilizando a formula Força = massa * aceleração
Unidade do retorno em Newton
*/
float force(float mass, float acceleration);

/*
Calculo do coeficiente de atrito  utilizando a formula de Força = Forca_atrito
                                             massa * aceleração = Mi * massa * gravidade
*/
float fatD(float force, float mass, float gravity);

/*
  Calculo da velocidade inicial do carrinho que ira percorrer a montanha russa
  Formula Q = massa * velocidade
  Retorna a velociadade em metros/segundos
*/
float initialSpeedCar(float mass, float massCar, float speedCar);

/*
Calculo da massa do carrinho que irá percorrer a montanha russa.
Retorna massa em Kilograma
*/
float findMass(float mass);


#endif