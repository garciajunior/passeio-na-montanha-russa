
#include "phisics.h"
#include <stdlib.h>
#include <math.h>
#include <string.h>

//Velociade em relação a altura
float speedGravity(float initialSpeed, float gravity, float height)
{
  return sqrt((pow(initialSpeed, 2)) + 2 * gravity * height);
}
//Energia Cinetica
float kineticEnergy(float mass, float speed)
{
  return ((mass * (pow(speed, 2))) / 2);
}
//Energia potencial gravitacional
float potentialEnergy(float mass, float gravity, float height)
{
  return (mass * gravity * height);
}

// Aceleração utilizando Torricelli
float accellerationPertorricelli(float initialSpeed, float finalSpeed, float deltaSpace)
{
  return ((pow(finalSpeed, 2) - (pow(initialSpeed, 2))) / (2 * deltaSpace)) * -1;
}

// tempo em segundos que o carrinho leva para parar o movimento.
float timeToStop(float initialSpeed, float finalSpeed, float acceleration)
{
  return ((initialSpeed + finalSpeed) / acceleration);
}

// Calculo da força
float force(float mass, float acceleration)
{
  return (mass * acceleration);
}

// Calculo do coeficiente de atrito
float fatD(float force, float mass, float gravity)
{
  return ((force) / (mass * gravity));
}

float initialSpeedCar(float mass, float massCar, float speedCar)
{
  return (massCar * speedCar) / (massCar + mass);
}

//Calculo da massa
float findMass(float mass)
{

  return mass + (mass * 0.25);
}