#include <stdio.h>
#include "phisics.h"
#include "message.h"
#include "game.h"

float question(char variable[10], float value);

int main()
{

  int continuar = 1;

  do
  {
    //Menu de opçoes
    printf("\nInicio do Jogo\n\n");
    printf("1. Inicio\n");
    printf("2. Criadores\n");
    printf("3. Sobre\n");
    printf("0. Sair\n");

    scanf("%d", &continuar);
    system("cls || clear");

    switch (continuar)
    {
    case 1:
      start();
      break;

    case 2:
      creators();
      break;

    case 3:
      about();
      break;

    case 0:
      logout();
      break;

    default:
      printf("Digite uma opcao valida\n");
    }
  } while (continuar);

  return 0;
}
