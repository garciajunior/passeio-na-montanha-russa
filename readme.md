# Simulação: passeio na montanha russa

## Proposta

> Uma programa simples (em C, ou pseudocódigo) que simule o trajeto de um carrinho
> numa “montanha russa" com um perfil esquematizado a seguir.

## Cenário

> Tem-se uma “montanha russa” especial, onde descidas e subidas exigem adaptações na
> massa do carrinho (pontos B e F) e, da aceleração gravitacional no trecho C-D para utilizar o
> principio da conservação de energia mecânica e garantir a manutenção do movimento.

## Jogo:

> O inicio do passeio ocorre após uma colisão elástica entre carrinhos no ponto A, vide perfil da
> montanha. O carrinho em movimento uniforme (da esquerda para direita) tem massa M e
> velocidade V quando colide com o outro carrinho em repouso no ponto A e, com massa 25% maior
> que o primeiro.

> Após o choque, o carrinho desce o primeiro trecho da montanha (pontos A a B), sendo necessário
> um ajuste instantâneo (ou mágico se preferir!) da massa para alcançar o ponto C que está acima do
> ponto A. No trecho seguinte, C a D, deve-se modificar a aceleração gravitacional (possível por um
> “arranjo experimental” no trecho), um efeito necessário para que a velocidade no ponto D seja
> compatível para para igualar a energia mecânica no ponto E.

> O trecho seguinte, E a F volta a ter a aceleração da gravidade normalizada e uma nova atualização
> da massa em F deve possibilitar atingir o ponto G e realizar a última descida.
> Note que os ajustes devem propiciar uma velocidade nula nos topos, sob pena do carrinho
> “descolar” e cair da montanha. No trecho final (a partir do ponto H) um freio com a inclusão de um
> atrito (simulando a colocação de areia nos trilhos) deve parar o carrinho no final do percurso entre
> pontos H e I.

> Para aceleração da gravidade adote g = 10 m/s2
> , e os valores da massa M, velocidade V e altura
> referencial H (altura entre os pontos A e B) são indicadas pelo usuário do programa ao inicializar.

> A intenção básica do jogo é usar conceitos de colisões em sistemas isolados e, de conservação de
> energia na maior parte do trajeto (pontos A até H) onde não há dissipação de energia, e para
> finalizar o movimento, a energia cinética restante deverá ser totalmente dissipada, já que o
> carrinho deve frear até parar (entre os pontos H e I).

### Tarefa

> Projetar o programa que simula um “passeio na montanha russa” do carrinho (inicialmente
> em A) que faz a movimentação, interagindo com o jogador para realizar os ajustes de massa e
> aceleração gravitacional, para manter-se nos trilhos e chegar ao final da montanha no ponto I.

<p align="center">
  <img src="montanha.png" width="350" 
</p>

# Execução do jogo

> Clonar o repositório

### Compilar via terminal

```shell
  gcc -Wall -o prog main.c phisics.c message.c game.c -lm

```
