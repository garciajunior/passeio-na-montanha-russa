#ifndef MESSAGE_H_
#define MESSAGE_H_
#include <stdlib.h>

  //Imprime uma montanha russa em ascii
  void printRoller();

  // imprime o nome do jogo em ascii
  void gameName();

  //Mostra os criadores do jogo
  void creators();

  //Historia do Jogo
  void about();

  // Sair do Jogo
  void logout();

#endif