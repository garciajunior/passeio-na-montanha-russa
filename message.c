#include <stdio.h>
#include "message.h"
void printRoller()
{

  printf(".  .  . .  .  . .  . .  .  . .  . .  .  . .  ,&@@.@#/# . #&@(  .  .  \n");
  printf(".  .  . .  .  . .  . .  .  . .  .    .  .    @@.              @ *    \n");
  printf(".  .  . .  .  . .  . .  .  . .  . .  .  . ,@@@@                (.* ..\n");
  printf(".  .  . .  .  . .  . .  .  . .  . .  .   ,(@$@@                (& . .\n");
  printf(".  .  . .  .  . .  . .  .  . .  . .  . ..@@ @.                  *  ..\n");
  printf("               .@ @ @.@,                 @@@#&                   @   \n");
  printf("               .@     @ @                 ,&                    @*   \n");
  printf("               .@         @              &,                      @   \n");
  printf("  . .  .  . .  .@           @. .  .  . &                         *   \n");
  printf("  . .  .  . .  .@           @.@@@/(@@@&                          *   \n");
  printf("  . .  .  . .  .@                                               ,* . \n");
  printf("  . .  .  . .  .@                                                &   \n");
  printf("  . .  .  . .  .@ ( (.@ , & @. *# .( &(. (.@*, # &./.@ # &.#.. & *   \n");
  printf("  . .  .  . .  .  . .  . .  . .  .  . .  .  . .  . .  .  . .  .    . \n ");
}

void gameName()
{
  printf("8888888b.                                      d8b                                    \n");
  printf("888   Y88b                                     Y8P                                    \n");
  printf("888    888                                                                            \n");
  printf("888   d88P  8888b.  .d8888b  .d8888b   .d88b.  888  .d88b.      88888b.   8888b.      \n");
  printf("8888888P        88b 88K      88K      d8P  Y8b 888 d88  88b     888  88b      88b     \n");
  printf("888        .d888888  Y8888b.  Y8888b. 88888888 888 888  888     888  888 .d888888     \n");
  printf("888        888  888      X88      X88 Y8b.     888 Y88..88P     888  888 888  888     \n");
  printf("888         Y888888  88888P   88888P    Y8888  888   Y88P       888  888  Y888888     \n");
  printf("\n");
  printf("888b     d888                   888                      888                   8888888b.                                      \n");
  printf("8888b   d8888                   888                      888                   888   Y88b                                     \n");
  printf("88888b.d88888                   888                      888                   888    888                                     \n");
  printf("888Y88888P888  .d88b.  88888b.  888888  8888b.  88888b.  88888b.   8888b.      888   d88P 888  888 .d8888b  .d8888b   8888b.  \n");
  printf("888 Y888P 888 d88  88b 888  88b 888         88b 888  88b 888  88b      88b     8888888P   888  888 88K      88K           88b \n");
  printf("888  Y8P  888 888  888 888  888 888    .d888888 888  888 888  888 .d888888     888 T88b   888  888  Y8888b.  Y8888b. .d888888 \n");
  printf("888       888 Y88..88P 888  888 Y88b.  888  888 888  888 888  888 888  888     888  T88b  Y88b 888      X88      X88 888  888 \n");
  printf("888       888   Y88P   888  888   Y888  Y888888 888  888 888  888  Y888888     888   T88b   Y88888  88888P   88888P   Y888888 \n");
  printf("##############################################################################################################################\n");
}

void creators()
{

  printf("8888888b                                                         888                        888                                    \n");
  printf("888   Y88b                                                       888                        888                                    \n");
  printf("888    888                                                       888                        888                                    \n");
  printf("888    888   d88b    d8888b    d88b   88888b   888  888   d88b   888 888  888   d88b     d88888   d88b   888d888   d88b    d8888b  \n");
  printf("888    888 d8P  Y8b 88K      d8P  Y8b 888  88b 888  888 d88  88b 888 888  888 d8P  Y8b d88  888 d88  88b 888P    d8P  Y8b 88K      \n");
  printf("888    888 88888888  Y8888b  88888888 888  888 Y88  88P 888  888 888 Y88  88P 88888888 888  888 888  888 888     88888888  Y8888b  \n");
  printf("888   d88P Y8b           X88 Y8b      888  888  Y8bd8P  Y88  88P 888  Y8bd8P  Y8b      Y88b 888 Y88  88P 888     Y8b           X88 \n");
  printf("8888888P     Y8888   88888P    Y8888  888  888   Y88P     Y88P   888   Y88P     Y8888    Y88888   Y88P   888       Y8888   88888P  \n");

  printf("###################################################################################################################################\n");
  printf("ANTONIO JOSE GARCIA JUNIOR – 34719\n");
  printf("ISAIAS MENDES MACHADO – 2017006498\n");
  printf("DENIS SOUZA DA ROSA – 2017017375\n");
  printf("THIAGO AUGUSTO DA SILVA CORTEZ – 2017005276\n");
}
void about()
{
  printf("Inicio do passeio ocorre após uma colisão elástica entre carrinhos no ponto A, vide perfil da ");
  printf("montanha. O carrinho em movimento uniforme (da esquerda para direita) tem massa M e\n");
  printf("velocidade V quando colide com o outro carrinho em repouso no ponto A e, com massa 25 por cento maior\n");
  printf("que o primeiro. \n");
}

void logout()
{
  system("cls || clear");
  printf("Saindo .... te vejo em breve");
  exit(1);
}